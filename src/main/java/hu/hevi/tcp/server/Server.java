package hu.hevi.tcp.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private static final int BUFSIZE = 32; // Size of receive buffer

    public static void main(String[] args) throws IOException {
        String data = "Toobie ornaught toobie";
        try {
            ServerSocket srvr = new ServerSocket(1234);
            Socket skt = srvr.accept();
            System.out.print("Server has connected!\n");
            PrintWriter out = new PrintWriter(skt.getOutputStream(), true);
            System.out.print("Sending string: '" + data + "'\n\r" + data);
            out.print(data + "\n\r" + data);
            out.close();
            skt.close();
            srvr.close();
        } catch (Exception e) {
            System.out.print("Whoops! It didn't work!\n");
        }
    }

}
